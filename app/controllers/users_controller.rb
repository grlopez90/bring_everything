class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy] 

  # GET /users.json
  def index
    @users = User.all
    
    respond_to do |format|
      format.json { render json: @users, status: :ok}
    end
  end

  # GET /users/1.json
  def show    
    # Value of user was set by the set_user method called by before_action
    respond_to do |format|
      format.json { render json: @user, status: :ok}
    end
  end 

  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save        
        format.json { render json: @user, status: :created }
      else        
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.json { render json: @user, status: :ok }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Allows me to use @user in action methods instead of repeating the code for finding the apprpriate user each time
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow parameters matching the user model
    def user_params
      params.fetch(:user, {})
    end
end
